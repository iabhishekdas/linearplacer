#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <fstream>
#include "types.h"
#include "read_files.h"
#include "globals.h"

using namespace std;

void globalMove()
{
	// Go column-wise from left to right
	for (int i = 0; i < width; i++)
	{
		// Iterate through each site to see if it is occupied
		for (int j = 0; j < height; j++)
		{
			// If there is a cell in the site and it is movable take action
			if (sites[index(i,j)].is_occupied)
			{
				if (cells[sites[index(i,j)].cell_no].is_movable)
				{
					// Store cell-index to avoid long names
					int current_cell_no = sites[index(i,j)].cell_no;
					enum e_block_types cellType = cells[current_cell_no].type;

					for(std::vector<int>::iterator it = cells[current_cell_no].nets_in_cell.begin();
							it != cells[current_cell_no].nets_in_cell.end(); ++it)
					{
							// Set y-bounds to default
						nets[*it].bounding_box.ymin = height;
						nets[*it].bounding_box.ymax = 0;
						nets[*it].bounding_box.xmin = width;
						nets[*it].bounding_box.xmax = 0;

						//cout << nets[*it].name << " ";
						// Compute bounding box without current cell
						for (int k = 0; k < nets[*it].num_cells; k++)
						{
							if (cells[nets[*it].blocks[k]].name == current_cell_no) continue;
							// Update bounding box
							nets[*it].bounding_box.ymin = min(cells[nets[*it].blocks[k]].y, nets[*it].bounding_box.ymin);
							nets[*it].bounding_box.ymax = max(cells[nets[*it].blocks[k]].y, nets[*it].bounding_box.ymax);
							nets[*it].bounding_box.xmin = min(cells[nets[*it].blocks[k]].x, nets[*it].bounding_box.xmin);
							nets[*it].bounding_box.xmax = max(cells[nets[*it].blocks[k]].x, nets[*it].bounding_box.xmax);
						}
					}

					// Store bounding box values in x and y vectors
					std::vector<int> x, y;
					for(std::vector<int>::iterator it = cells[current_cell_no].nets_in_cell.begin();
						it != cells[current_cell_no].nets_in_cell.end(); ++it)
					{
						x.push_back(nets[*it].bounding_box.xmin);
						x.push_back(nets[*it].bounding_box.xmax);
						y.push_back(nets[*it].bounding_box.ymin);
						y.push_back(nets[*it].bounding_box.ymax);
					}
			
					// Compute median values for x and y
					std::sort(x.begin(), x.end());
					int x2 = x[x.size()/2]; int x1 = x[(x.size()/2)-1]; 
					std::sort(y.begin(), y.end()); 
					int y2 = y[y.size()/2]; int y1 = y[(y.size()/2)-1];
					//cout << "Optimal Region: " << x1 << "," << x2 << "," << y1 << "," << y2 << " ";

					// Check which cell is empty in optimal region
					int xopt = i, yopt = j; 
					bool found_loc = false;
					for (int optx = x1; optx <= x2; optx++) 
					{ 
						for (int opty = y1; opty <= y2; opty++)
						{
							if ((!sites[index(optx, opty)].is_occupied) && (sites[index(optx, opty)].type == cellType)) 
							{ xopt = optx; yopt = opty; found_loc = true; goto loopend; }
						}
					}
					
					loopend:
					if (found_loc) {
						// If empty location found move it
						cells[current_cell_no].x = xopt; cells[current_cell_no].y = yopt;
						sites[index(i, j)].is_occupied = false;
						sites[index(xopt, yopt)].is_occupied = true;
						sites[index(i, j)].cell_no = 0;
						sites[index(xopt, yopt)].cell_no = current_cell_no;
						//cout << "Empty Location: " << xopt << "," << yopt << " <-- " << i << "," << j << endl; 
					}
					/*else {
						// If no empty location found, swap with each cell and keep the one with the first better HPWL
						for (int optx = x1; optx <= x2; optx++) 
						{ 
							for (int opty = y1; opty <= y2; opty++)
							{
								if (sites[index(optx,opty)].type != cellType) continue;
								int cell_no = sites[index(optx,opty)].cell_no;
								if (!cells[cell_no].is_movable) continue;

								// Compute HPWL of cell to be swapped
								update_bb(cell_no);
								int hpwl2 = calc_hpwl(cell_no);
								//cout << "Swap Cell: " << cell_no << " hpwl=" << hpwl2 << " ";

								// Swap cell
								cells[cell_no].x = i; cells[cell_no].y = j;
								cells[current_cell_no].x = optx; cells[current_cell_no].y = opty;

								// Update bounding boxes
								update_bb(current_cell_no); update_bb(cell_no);

								// Compute new total HPWL 
								int swapHPWL = calc_hpwl(cell_no);
								//cout << "new hpwl= " << swapHPWL << " loc=" << optx << "," << opty << endl;

								// Compare HPWL and revert back if no improvement
								if ( swapHPWL >= hpwl2 ) {
									cells[cell_no].x = optx; cells[cell_no].y = opty;
									cells[current_cell_no].x = i; cells[current_cell_no].y = j;
								}
								else {
									sites[index(optx, opty)].cell_no = current_cell_no;
									sites[index(i, j)].cell_no = cell_no;
									goto loopend2;
								}
							}
						}
					}
					loopend2:; //NOP*/
				}
			}
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 5) std::cerr << "-E- ./globalSwap <sitemap_file> <input_placement_file> <netlist> <output_file>" << endl;
	else
	{
		cout << "-I- Reading SiteMap... ";
		update_siteMap(argv[1]);
		cout << "-I- Reading Initial Placement... "; 
		read_initial_placement(argv[2]);
		cout << "-I- Reading netlist... ";
		read_netlist(argv[3]);
		cout << "-I- Peforming Global Move... ";
		
		globalMove();
		cout << "DONE" << "\n-I- Writing to output file... ";

		ofstream outfile;
		outfile.open(argv[4]); //"../examples/Detailed-Placement.pl");

		// Write output to file
		for (int i = 0; i < no_of_cells; i++)
		{
			outfile << "cell_" << cells[i].name;
			if (cells[i].type == CLB) 	outfile << " CLB ";
			else if (cells[i].type == DSP) 	outfile << " DSP ";
			else if (cells[i].type == RAM) 	outfile << " RAM ";
			else if (cells[i].type == IO) 	outfile << " IO ";
			outfile << cells[i].x << " " << cells[i].y;
			if (cells[i].is_movable) outfile << " M\n";
			else outfile << " F\n";
		} 	

		outfile.close();
		cout << "DONE" << endl;
	}
  	return 0;
}
