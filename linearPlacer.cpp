#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <fstream>
#include "types.h"
#include "read_files.h"
#include "globals.h"

using namespace std;

void linear_place()
{
	// back-up copy of cell to be moved
	struct s_block copy_cell;

	// HPWL back-up copy
	int hpwl;

	// Window range
	int wmin, wmax;

	// Go column-wise from left to right
	for (int i = 0; i < width; i++)
	{
		// Iterate through each site to see if it is occupied
		for (int j = 0; j < height; j++)
		{
			// If there is a cell in the site and it is movable take action
			if (sites[index(i,j)].is_occupied)
			{
				if (cells[sites[index(i,j)].cell_no].is_movable)
				{
					// Store cell-index to avoid long names
					int current_cell_no = sites[index(i,j)].cell_no;
	
					// Find current HPWL
					hpwl = 0;
					for(std::vector<int>::iterator it = cells[current_cell_no].nets_in_cell.begin(); it != cells[current_cell_no].nets_in_cell.end(); ++it)
						hpwl += compute_hpwl(*it); 

					// Find movable range
					wmin = cells[current_cell_no].y;
					wmax = cells[current_cell_no].y;
					bool min_found = false, max_found = false;
					
					// Continue till both the min. and max. of window range found
					while (!(min_found && max_found))
					{
						// Search if site downwards are valid or not
						if (sites[index(i,wmin-1)].is_occupied || (wmin < 1)) min_found = true;
						else wmin--;

						// Search if site upwards are valid or not
						if (sites[index(i,wmax+1)].is_occupied || (wmax >= (height-1))) max_found = true;
						else wmax++;
			
					}
					
					// Now for each available site within window check the following things:
					// 1. If site is of same type as cell
					// 2. If type is same, check whether it causes reduction in total HPWL for associated nets
					// Need to change/optimize just the y-coordinate of cell

					int orig_hpwl = hpwl; int orig_y = cells[current_cell_no].y;
					int best_hpwl = hpwl; int best_y = orig_y;
					
					for (int yc = wmin; yc <= wmax; yc++)
					{
						// Check if type matches or not, no point continuing if not!
						if (sites[index(i,yc)].type != cells[current_cell_no].type) continue;
						
						int new_hpwl = 0;
						cells[current_cell_no].y = yc;

						for(std::vector<int>::iterator it = cells[current_cell_no].nets_in_cell.begin(); 
							it != cells[current_cell_no].nets_in_cell.end(); ++it)
						{
							// Set y-bounds to default
							nets[*it].bounding_box.ymin = height;
							nets[*it].bounding_box.ymax = 0;

							for (int k = 0; k < nets[*it].num_cells; k++)  
							{
								// Update bounding box 
								nets[*it].bounding_box.ymin = min(cells[nets[*it].blocks[k]].y, nets[*it].bounding_box.ymin);
								nets[*it].bounding_box.ymax = max(cells[nets[*it].blocks[k]].y, nets[*it].bounding_box.ymax);
							}
							//int c = compute_hpwl(*it); cout << current_cell_no << "\t" << yc << "\t" << nets[*it].name << "\t" << c << endl;
							new_hpwl += compute_hpwl(*it);
						}

						if (new_hpwl < best_hpwl) { best_hpwl = new_hpwl; best_y = yc; } 
					}
					//cout << "Cell_" << current_cell_no << "\t" << orig_y << "\t" << hpwl << "\t" << best_y << "\t" << best_hpwl << endl;
			
					// Move cell to best location
					cells[current_cell_no].y = best_y;

					// Mark site as occupied and previous site as unoccupied
					sites[index(i,orig_y)].is_occupied = false;
					sites[index(i,orig_y)].cell_no = 0;
					sites[index(i,best_y)].is_occupied = true;
					sites[index(i,best_y)].cell_no = current_cell_no;
				}
			}
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 5) std::cerr << "-E- ./linearPlacer <sitemap_file> <input_placement_file> <netlist> <output_file>" << endl;
	else
	{
		cout << "-I- Reading SiteMap... ";
		update_siteMap(argv[1]);
		cout << "-I- Reading Initial Placement... "; 
		read_initial_placement(argv[2]);
		cout << "-I- Reading netlist... ";
		read_netlist(argv[3]);
		cout << "-I- Peforming Linear Placement ColumnWise... ";
		
		linear_place();
		cout << "DONE" << "\n-I- Writing to output file... ";

		ofstream outfile;
		outfile.open(argv[4]); //"../examples/Detailed-Placement.pl");

		// Write output to file
		for (int i = 0; i < no_of_cells; i++)
		{
			outfile << "cell_" << cells[i].name;
			if (cells[i].type == CLB) 	outfile << " CLB ";
			else if (cells[i].type == DSP) 	outfile << " DSP ";
			else if (cells[i].type == RAM) 	outfile << " RAM ";
			else if (cells[i].type == IO) 	outfile << " IO ";
			outfile << cells[i].x << " " << cells[i].y;
			if (cells[i].is_movable) outfile << " M\n";
			else outfile << " F\n";
		} 	

		outfile.close();
		cout << "DONE" << endl;
		// Test case
		// cout << nets[100].name << ": (" << nets[100].bounding_box.xmin << ", " << nets[100].bounding_box.ymin << ") (" << nets[100].bounding_box.xmax << ", " 
		//	<< nets[100].bounding_box.ymax << ")" << endl;
		// for(std::vector<int>::iterator it = cells[100].nets_in_cell.begin(); it != cells[100].nets_in_cell.end(); ++it) 
		//	cout << *it << ", " ;
		// cout << "\nTotal nets: " << cells[100].nets_in_cell.size() << endl;  
	}
  	return 0;
}
