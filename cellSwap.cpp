#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <fstream>
#include "types.h"
#include "read_files.h"
#include "globals.h"

using namespace std;

void updateBB( int current_cell_no )
{
	// Update bounding box (only y-coordinate)
	for(std::vector<int>::iterator itt = cells[current_cell_no].nets_in_cell.begin(); 
		itt != cells[current_cell_no].nets_in_cell.end(); ++itt)
	{
		// Set y-bounds to default
		nets[*itt].bounding_box.ymin = height;
		nets[*itt].bounding_box.ymax = 0;

		for (int k = 0; k < nets[*itt].num_cells; k++)  
		{
			// Update bounding box 
			nets[*itt].bounding_box.ymin = min(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymin);
			nets[*itt].bounding_box.ymax = max(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymax);
		}
	}
}

void cellSwap(int N)
{
	// Go column-wise from left to right
	for (int i = 0; i < width; i++)
	{
		int j = 0;
		// Iterate through each row to see if it is occupied
		while (j < height)
		{
			// Set maximum limit for window	
			int limit = min((j+N), height);

			// Store cells in Queue
			for (int c = j; c < limit; c++)
			{
				// If there is a cell in the site and it is movable store in Queue (fixed cells are not moved)
				if (sites[index(i,c)].is_occupied && cells[sites[index(i,c)].cell_no].is_movable)
				{
					int current_cell = sites[index(i,c)].cell_no;
			
					for (int cc = c+1; cc < limit; cc++)
					{
						if (sites[index(i,cc)].type != cells[sites[index(i,c)].cell_no].type) continue;
						if (sites[index(i,cc)].is_occupied && cells[sites[index(i,cc)].cell_no].is_movable)
						{
					
							int swap_cell = sites[index(i,cc)].cell_no;
							cells[current_cell].y = cc;
							cells[swap_cell].y = c;
							sites[index(i,c)].cell_no = swap_cell;
							sites[index(i,cc)].cell_no = current_cell;

							int HPWLa = calc_hpwl(swap_cell);
							int HPWLb = calc_hpwl(current_cell);

							// Update Y-bounding box
							updateBB(swap_cell); updateBB(current_cell);

							if ((calc_hpwl(swap_cell) >= HPWLa) || (calc_hpwl(current_cell) >= HPWLb)) {
								// Revert back
								cells[current_cell].y = c; cells[swap_cell].y = cc;
								sites[index(i,cc)].cell_no = swap_cell;
								sites[index(i,c)].cell_no = current_cell;
								updateBB(swap_cell); updateBB(current_cell);
							}
							else cc = limit;
					 	}
					}
				}
			}
			j = j + N;
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 6) std::cerr << "-E- ./cellSwap <sitemap_file> <input_placement_file> <netlist> <output_file> <window_size>" << endl;
	else
	{
		cout << "-I- Reading SiteMap... ";
		update_siteMap(argv[1]);
		cout << "-I- Reading Initial Placement... "; 
		read_initial_placement(argv[2]);
		cout << "-I- Reading netlist... ";
		read_netlist(argv[3]);
		cout << "-I- Peforming cell swapping... ";
		cellSwap(atoi(argv[5]));
		cout << "DONE" << "\n-I- Writing to output file... ";

		ofstream outfile;
		outfile.open(argv[4]); //"../examples/Detailed-Placement.pl");

		// Write output to file
		for (int i = 0; i < no_of_cells; i++)
		{
			outfile << "cell_" << cells[i].name;
			if (cells[i].type == CLB) 	outfile << " CLB ";
			else if (cells[i].type == DSP) 	outfile << " DSP ";
			else if (cells[i].type == RAM) 	outfile << " RAM ";
			else if (cells[i].type == IO) 	outfile << " IO ";
			outfile << cells[i].x << " " << cells[i].y;
			if (cells[i].is_movable) outfile << " M\n";
			else outfile << " F\n";
		} 	

		outfile.close();
		cout << "DONE" << endl;
	}
  	return 0;
}
