#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include "types.h"
#include "read_files.h"
#include "globals.h"


using namespace std;

// Definition of global variables
int width;
int height;
int no_of_cells;
int no_of_nets;
struct s_site *sites;
struct s_block *cells;
struct s_net *nets;

// Indexing technique to map 2D-array to 1D-array
int index(int x, int y) { return ((x*height) + y); }


// Reads sitemap file and stores it in an array of structs
void update_siteMap(char *str)
{
	string line;

	//Open SiteMap file
	ifstream siteMap (str); //"../examples/FPGA-example1.sitemap");
  	if (siteMap.is_open())
  	{
		// Pointer for de-limiter
		size_t pos;		

		// Read from first line dimensions of site
		if (getline(siteMap, line)) 
		{
			pos = line.find(" ");
			width = atoi(line.substr(0, pos).c_str());
			height = atoi(line.substr(pos+1, line.length() - 1).c_str());
			cout << width << " x " << height << endl;
		}	
    	
		// Create sites
		sites = new struct s_site[width*height];
			
		// Read each location and update the sitemap
		while (getline (siteMap,line))
    		{
			// Location
      			pos = line.find(" ");
			int x = atoi(line.substr(0, pos).c_str());
			line.erase(0, pos+1);
      			pos = line.find(" ");
			int y = atoi(line.substr(0, pos).c_str());
			sites[index(x,y)].x = x;
			sites[index(x,y)].y = y;

			//Type of site
			string type = line.substr(pos+1, line.length() - 1);
			if (type.compare("CLB") == 0) sites[index(x,y)].type = CLB;
			else if (type.compare("RAM") == 0) sites[index(x,y)].type = RAM;
			else if (type.compare("DSP") == 0) sites[index(x,y)].type = DSP;
			else if (type.compare("IO") == 0) sites[index(x,y)].type = IO;
			else if (type.compare("INVALID") == 0) sites[index(x,y)].type = INVALID;

			//Mark as unoccupied
			sites[index(x,y)].is_occupied = false;

			//Initialize cell no. to 0
			sites[index(x,y)].cell_no = 0;
    		}
	
		// Check SiteMap
		// for (int j = 0; j < i; j++) cout << sites[j].x << " " << sites[j].y << " " << sites[j].type << " " << sites[j].is_occupied << endl; 

    		siteMap.close();
  	}
  	else cout << "Unable to open file"; 

}

void read_initial_placement(char *str)
{
	string line;
	no_of_cells = 0;

	// Open placement file
	ifstream pl (str); //"../examples/FPGA-example1.pl");
  	if (pl.is_open())
  	{
		// First Need to know how many cells are there in design, calculate no. of cells	
		while (getline (pl,line))
			if ( line.find("cell_") != string::npos ) no_of_cells++;

    		pl.close();
  	}
  	else cout << "Unable to open file";

	cout << "No. of cells: " << no_of_cells << endl;
	cells = new struct s_block[no_of_cells];

	// Open placement file again and this time store the cell info
	pl.open(str); //"../examples/FPGA-example1.pl");
  	if (pl.is_open())
  	{
		size_t pos;

		// Get cell info line by line
		while (getline (pl,line))
		{
			// Assuming every cell number starts with cell_N, we extract N by taking int of whatever is in position 5 to first space
			pos = line.find(" ");
			int cell_no = atoi(line.substr(5, pos).c_str());
			line.erase(0,pos+1);

			// Cell name is the cell no. only
			cells[cell_no].name = cell_no;

			// Type of cell
			pos = line.find(" ");
			string type = line.substr(0, pos);
			line.erase(0,pos+1);
			if (type.compare("CLB") == 0) cells[cell_no].type = CLB;
			else if (type.compare("RAM") == 0) cells[cell_no].type = RAM;
			else if (type.compare("DSP") == 0) cells[cell_no].type = DSP;
			else if (type.compare("IO") == 0) cells[cell_no].type = IO;
				
			// x-coordinate
			pos = line.find(" ");
			cells[cell_no].x = atoi(line.substr(0, pos).c_str());
			line.erase(0,pos+1);
				
			// y-coordinate
			pos = line.find(" ");
			cells[cell_no].y = atoi(line.substr(0, pos).c_str());

			// Movable or not
			string m_f = line.substr(pos+1, 1);
			if (m_f.compare("M") == 0) cells[cell_no].is_movable = true;
			else if (m_f.compare("F") == 0) cells[cell_no].is_movable = false;

			// Mark location as occupied in sitemap
			sites[index(cells[cell_no].x, cells[cell_no].y)].is_occupied = true;

			// Update cell no. in sitemap for location
			sites[index(cells[cell_no].x, cells[cell_no].y)].cell_no = cell_no;
		}

		// Check cells
		// for (int j = 0; j < no_of_cells; j++) 
		//	cout << cells[j].name << " " << cells[j].type << " " << cells[j].x << " " << cells[j].y << " " << cells[j].is_movable << endl;
    		
		pl.close();
  	}
  	else cout << "Unable to open file";
	
}

void read_netlist(char *str)
{
	string line;
	no_of_nets = 0;
	int i = 0;

	// Open placement file
	ifstream netlist (str); //"../examples/FPGA-example1.nets");
  	if (netlist.is_open())
  	{
		// First Need to know how many nets are there in design, calculate no. of cells	
		// Each line is assumed to be a net from netlist
		while (getline (netlist,line))
			no_of_nets++;

    		netlist.close();
  	}
  	else cout << "Unable to open file";

	cout << "No. of nets: " << no_of_nets << endl;
	nets = new struct s_net[no_of_nets];

	// Open netlist again and store net info
	netlist.open(str); //"../examples/FPGA-example1.nets");
	if (netlist.is_open())
	{
		size_t pos = 0;
		// The first word is the net name, followed by all the cells connected to it.
		while (getline (netlist,line))
		{
			// Get net name
			pos = line.find(" ");
			nets[i].name = line.substr(0, pos);
			line.erase(0, pos+1);	 
			
			string cells_in_current_net = line;
		
			pos = 0; 
			nets[i].num_cells = 0;
			// Get no_of_cells connected to net
			while ( pos != string::npos) 
			{
				pos = line.find(" ");
				nets[i].num_cells++;
				line.erase(0, pos+1);
			}

			nets[i].blocks = new int[nets[i].num_cells];
			int c = 0;
			pos = 0; 
			// Store all cell no.s connected to net
			while (pos != string::npos)
			{
				pos = cells_in_current_net.find(" ");
				nets[i].blocks[c++] = atoi(cells_in_current_net.substr(5, pos).c_str());
				cells_in_current_net.erase(0, pos+1);
			}

			i++;
		}
				
    		netlist.close();
  	}
  	else cout << "Unable to open file";

	// Update bounding box for each net and update the number of nets a cell is part of
	for (int j = 0; j < no_of_nets; j++) 
	{ 
		nets[j].bounding_box.xmin = height;
		nets[j].bounding_box.ymin = width;
		nets[j].bounding_box.xmax = 0;
		nets[j].bounding_box.ymax = 0;
		for (int k = 0; k < nets[j].num_cells; k++)  
		{
			// Add current net to vector of nets part of the cell
			cells[nets[j].blocks[k]].nets_in_cell.push_back(j);

			// Update bounding box 
			if (cells[nets[j].blocks[k]].x < nets[j].bounding_box.xmin) nets[j].bounding_box.xmin = cells[nets[j].blocks[k]].x;	 
			if (cells[nets[j].blocks[k]].y < nets[j].bounding_box.ymin) nets[j].bounding_box.ymin = cells[nets[j].blocks[k]].y;	 
			if (cells[nets[j].blocks[k]].x > nets[j].bounding_box.xmax) nets[j].bounding_box.xmax = cells[nets[j].blocks[k]].x;	 
			if (cells[nets[j].blocks[k]].y > nets[j].bounding_box.ymax) nets[j].bounding_box.ymax = cells[nets[j].blocks[k]].y;
		}
	}	 
		
}




