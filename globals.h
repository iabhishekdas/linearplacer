#ifndef GLOBALS_H_
#define GLOBALS_H_

#include <iostream>

// Globals -> no need to pass this everywhere
extern int width;
extern int height;
extern int no_of_cells;
extern int no_of_nets;
extern struct s_site *sites;
extern struct s_block *cells;
extern struct s_net *nets;

#define compute_hpwl(i) abs(nets[i].bounding_box.xmax - nets[i].bounding_box.xmin) + abs(nets[i].bounding_box.ymax - nets[i].bounding_box.ymin);
#define min(a,b)	(a < b) ? a : b
#define max(a,b)	(a > b) ? a : b

int calc_hpwl(int current_cell_no);
void update_bb(int current_cell_no);

#endif
