#ifndef READ_FILES_H_
#define READ_FILES_H_

#include <iostream>

int index(int x, int y);
void update_siteMap(char *str);
void read_initial_placement(char *str);
void read_netlist(char *str);

#endif
