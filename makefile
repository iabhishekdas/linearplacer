# the compiler: g++ for C++
CC = g++

# compiler flags:

# the build target executable:
TARGET1 = globalMove
TARGET2 = linearPlacer
TARGET3 = slidingWindow
TARGET4 = cellInterleaver 
TARGET5 = cellSwap

all: $(TARGET1) $(TARGET2) $(TARGET3) $(TARGET4) $(TARGET5)

$(TARGET1): globalMove.cpp read_files.cpp globals.cpp read_files.h globals.h types.h
	$(CC) -o $(TARGET1) globalMove.cpp read_files.cpp globals.cpp 

$(TARGET2): linearPlacer.cpp read_files.cpp globals.cpp read_files.h globals.h types.h
	$(CC) -o $(TARGET2) linearPlacer.cpp read_files.cpp globals.cpp 

$(TARGET3): slidingWindow.cpp read_files.cpp globals.cpp read_files.h globals.h types.h
	$(CC) -o $(TARGET3) slidingWindow.cpp read_files.cpp globals.cpp 

$(TARGET4): cellInterleaver.cpp read_files.cpp globals.cpp read_files.h globals.h types.h
	$(CC) -o $(TARGET4) cellInterleaver.cpp read_files.cpp globals.cpp 

$(TARGET5): cellSwap.cpp read_files.cpp globals.cpp read_files.h globals.h types.h
	$(CC) -o $(TARGET5) cellSwap.cpp read_files.cpp globals.cpp 

clean:
	$(RM) $(TARGET1) $(TARGET2) $(TARGET3) $(TARGET4) $(TARGET5)
