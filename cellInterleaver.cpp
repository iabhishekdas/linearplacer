#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <fstream>
#include "types.h"
#include "read_files.h"
#include "globals.h"

using namespace std;

void cellInterleaver(int N)
{
	// Go column-wise from left to right
	for (int i = 0; i < width; i++)
	{
		int j = 0;
		// Iterate through each row to see if it is occupied
		while (j < height)
		{
			// Queue to store cells in each window
			std::queue<int> QueueA, QueueB, QueuePos;
				
			// Set maximum limit for window	
			int limit = min((j+N), height);

			int cnt = 0;
			// Store cells in Queues; odd in QueueA, even in QueueB
			for (int c = j; c < limit; c++)
			{
				// If there is a cell in the site and it is movable store in Queue (fixed cells are not moved)
				if (sites[index(i,c)].is_occupied)
				{
					if (cells[sites[index(i,c)].cell_no].is_movable)
					{
						if (cnt % 3 == 0) QueueA.push(sites[index(i,c)].cell_no);
						else QueueB.push(sites[index(i,c)].cell_no);
						cnt++;

						// Push Position into queue
						QueuePos.push(c);

						// Mark site as empty once stored in Queue
						//sites[index(i,c)].cell_no = 0;
					}  	
				}
			}
		
			// Pop each element and check for best position to place
			while (!QueuePos.empty())			
			{
				//Current Postiton
				int pos = QueuePos.front();
				QueuePos.pop();

				cout << "\n" << i << "," << pos << "\t" << QueueA.front() << "\t" << QueueB.front() << "\t" ;
				
				if (QueueB.empty()) { int ai = QueueA.front(); cells[ai].y = pos; QueueA.pop();  }
				else if (QueueA.empty()) { int bi = QueueB.front(); cells[bi].y = pos; QueueB.pop();  }
				else {
					int ai = QueueA.front();
					int bi = QueueB.front();
					int HPWLa = calc_hpwl(ai); int tempA = cells[ai].y;
					int HPWLb = calc_hpwl(bi); int tempB = cells[bi].y;

					// Compute new HPWL by placing both cells one by one in location
					cells[ai].y = pos; 	
					int current_cell_no = ai;

					// Update bounding box (only y-coordinate)
					for(std::vector<int>::iterator itt = cells[current_cell_no].nets_in_cell.begin(); 
						itt != cells[current_cell_no].nets_in_cell.end(); ++itt)
					{
						// Set y-bounds to default
						nets[*itt].bounding_box.ymin = height;
						nets[*itt].bounding_box.ymax = 0;

						for (int k = 0; k < nets[*itt].num_cells; k++)  
						{
							// Update bounding box 
							nets[*itt].bounding_box.ymin = min(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymin);
							nets[*itt].bounding_box.ymax = max(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymax);
						}
					}

					// Compute new HPWL and compare
					float diffA = (calc_hpwl(current_cell_no) - HPWLa)/(float)HPWLa;

					// Do same thing for B
					cells[ai].y = tempA;
					cells[bi].y = pos; 	
					current_cell_no = bi;

					// Update bounding box (only y-coordinate)
					for(std::vector<int>::iterator itt = cells[current_cell_no].nets_in_cell.begin(); 
						itt != cells[current_cell_no].nets_in_cell.end(); ++itt)
					{
						// Set y-bounds to default
						nets[*itt].bounding_box.ymin = height;
						nets[*itt].bounding_box.ymax = 0;

						for (int k = 0; k < nets[*itt].num_cells; k++)  
						{
							// Update bounding box 
							nets[*itt].bounding_box.ymin = min(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymin);
							nets[*itt].bounding_box.ymax = max(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymax);
						}
					}

					// Compute new HPWL and compare
					float diffB = (calc_hpwl(current_cell_no) - HPWLb)/(float)HPWLb;
					cout << diffA << "\t" << diffB << endl;

					// Compare and interleave, which is more -ve 
					if (diffA < diffB) // more -ve means smaller
					{
						cells[bi].y = tempB;
						cells[ai].y = pos;
						sites[index(i,pos)].cell_no = ai;
						QueueA.pop(); 
					} 
					else {
						cells[bi].y = pos;
						cells[ai].y = tempA;
						sites[index(i,pos)].cell_no = bi;
						QueueB.pop();
					} 
				}
			}
			j = j + N;
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 6) std::cerr << "-E- ./cellInterleaver sitemap_file> <input_placement_file> <netlist> <output_file> <window_size>" << endl;
	else
	{
		cout << "-I- Reading SiteMap... ";
		update_siteMap(argv[1]);
		cout << "-I- Reading Initial Placement... "; 
		read_initial_placement(argv[2]);
		cout << "-I- Reading netlist... ";
		read_netlist(argv[3]);
		cout << "-I- Peforming cell-interleaving... ";
		cellInterleaver(atoi(argv[5]));
		cout << "DONE" << "\n-I- Writing to output file... ";

		ofstream outfile;
		outfile.open(argv[4]); //"../examples/Detailed-Placement.pl");

		// Write output to file
		for (int i = 0; i < no_of_cells; i++)
		{
			outfile << "cell_" << cells[i].name;
			if (cells[i].type == CLB) 	outfile << " CLB ";
			else if (cells[i].type == DSP) 	outfile << " DSP ";
			else if (cells[i].type == RAM) 	outfile << " RAM ";
			else if (cells[i].type == IO) 	outfile << " IO ";
			outfile << cells[i].x << " " << cells[i].y;
			if (cells[i].is_movable) outfile << " M\n";
			else outfile << " F\n";
		} 	

		outfile.close();
		cout << "DONE" << endl;
	}
  	return 0;
}
