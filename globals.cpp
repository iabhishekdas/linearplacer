#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <fstream>
#include "types.h"
#include "globals.h"

using namespace std;

int calc_hpwl(int current_cell_no)
{
	int hpwl = 0;
	for(std::vector<int>::iterator it = cells[current_cell_no].nets_in_cell.begin(); it != cells[current_cell_no].nets_in_cell.end(); ++it)
		hpwl += compute_hpwl(*it);

	return hpwl;
}

void update_bb(int current_cell_no)
{
	for(std::vector<int>::iterator it = cells[current_cell_no].nets_in_cell.begin();
			it != cells[current_cell_no].nets_in_cell.end(); ++it)
	{
		// Set y-bounds to default
		nets[*it].bounding_box.ymin = height;
		nets[*it].bounding_box.ymax = 0;
		nets[*it].bounding_box.xmin = width;
		nets[*it].bounding_box.xmax = 0;

		// Compute bounding box without current cell
		for (int k = 0; k < nets[*it].num_cells; k++)
		{
			// Update bounding box
			nets[*it].bounding_box.ymin = min(cells[nets[*it].blocks[k]].y, nets[*it].bounding_box.ymin);
			nets[*it].bounding_box.ymax = max(cells[nets[*it].blocks[k]].y, nets[*it].bounding_box.ymax);
			nets[*it].bounding_box.xmin = min(cells[nets[*it].blocks[k]].x, nets[*it].bounding_box.xmin);
			nets[*it].bounding_box.xmax = max(cells[nets[*it].blocks[k]].x, nets[*it].bounding_box.xmax);
		}
		//cout << current_cell_no << ": " << nets[*it].name << " (" << nets[*it].bounding_box.xmin << ", " << nets[*it].bounding_box.ymin << \
		 ") (" << nets[*it].bounding_box.xmax << ", " << nets[*it].bounding_box.ymax << ")\n";
					
	}

}
