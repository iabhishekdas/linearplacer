#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <stdlib.h>
#include <fstream>
#include "types.h"
#include "read_files.h"
#include "globals.h"

using namespace std;

void slidingWindow(int N)
{
	// Go column-wise from left to right
	for (int i = 0; i < width; i++)
	{
		int j = 0;
		// Iterate through each row to see if it is occupied
		while (j < height)
		{
			// Queue to store cells in each window
			std::queue<int> Queue;
				
			// Set maximum limit for window	
			int limit = min((j+N), height);

			// Store cells in Queue
			for (int c = j; c < limit; c++)
			{
				// If there is a cell in the site and it is movable store in Queue (fixed cells are not moved)
				if (sites[index(i,c)].is_occupied)
				{
					if (cells[sites[index(i,c)].cell_no].is_movable)
					{
						Queue.push(sites[index(i,c)].cell_no);
						// Mark site as empty once stored in Queue
						sites[index(i,c)].is_occupied = false;
						sites[index(i,c)].cell_no = 0;
						//cout << sites[index(i,c)].cell_no;
					}  	
				}
			}
		
			//for (std::vector<int>::iterator it = Queue.begin(); it != Queue.end(); ++it) cout << Queue[*it] << " ";
			
			// Pop each element and check for best position to place
			while (!Queue.empty())			
			{
				// Current cell no
				int ccn = Queue.front();
				Queue.pop();
	
				// For HPWL comparison of cell
				int bestHPWL = width*height;
				int bestPos  = height;

				// Iterate through all positions in window
				for (int pos = j; pos < limit; pos++)
				{
					// Check if site is empty
					if (!sites[index(i,pos)].is_occupied)
					{
						// Move cell to empty site
						cells[ccn].y = pos;
						int current_cell_no = ccn;

						// Update bounding box (only y-coordinate)
						for(std::vector<int>::iterator itt = cells[current_cell_no].nets_in_cell.begin(); 
							itt != cells[current_cell_no].nets_in_cell.end(); ++itt)
						{
							// Set y-bounds to default
							nets[*itt].bounding_box.ymin = height;
							nets[*itt].bounding_box.ymax = 0;

							for (int k = 0; k < nets[*itt].num_cells; k++)  
							{
								// Update bounding box 
								nets[*itt].bounding_box.ymin = min(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymin);
								nets[*itt].bounding_box.ymax = max(cells[nets[*itt].blocks[k]].y, nets[*itt].bounding_box.ymax);
							}
						}

						// Compute new HPWL and compare
						int newHPWL = calc_hpwl(current_cell_no);

						// Compare HPWL and store if minimum
						if (newHPWL < bestHPWL) { bestHPWL = newHPWL; bestPos = pos; }
					}
				}

				// Move cell to best possible position
				cells[ccn].y = bestPos;

				// Mark the site as occupied and update its cell no
				sites[index(i,bestPos)].is_occupied = true;
				sites[index(i,bestPos)].cell_no = ccn;
			}
			j = j + N;
		}
	}
}

int main(int argc, char* argv[])
{
	if (argc != 6) std::cerr << "-E- ./slidingWindow <sitemap_file> <input_placement_file> <netlist> <output_file> <window_size>" << endl;
	else
	{
		cout << "-I- Reading SiteMap... ";
		update_siteMap(argv[1]);
		cout << "-I- Reading Initial Placement... "; 
		read_initial_placement(argv[2]);
		cout << "-I- Reading netlist... ";
		read_netlist(argv[3]);
		cout << "-I- Peforming sliding window placement... ";
		slidingWindow(atoi(argv[5]));
		cout << "DONE" << "\n-I- Writing to output file... ";

		ofstream outfile;
		outfile.open(argv[4]); //"../examples/Detailed-Placement.pl");

		// Write output to file
		for (int i = 0; i < no_of_cells; i++)
		{
			outfile << "cell_" << cells[i].name;
			if (cells[i].type == CLB) 	outfile << " CLB ";
			else if (cells[i].type == DSP) 	outfile << " DSP ";
			else if (cells[i].type == RAM) 	outfile << " RAM ";
			else if (cells[i].type == IO) 	outfile << " IO ";
			outfile << cells[i].x << " " << cells[i].y;
			if (cells[i].is_movable) outfile << " M\n";
			else outfile << " F\n";
		} 	

		outfile.close();
		cout << "DONE" << endl;
	}
  	return 0;
}
