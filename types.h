/* Block Types */
enum e_block_types {CLB, DSP, RAM, IO, INVALID};

// Store location and type of cell that can be placed in this location 
struct s_site { 
	int x; int y; 
	enum e_block_types type; 
	bool is_occupied;
	int cell_no; 
};

// Bounding Box
struct s_bb {
	int xmin; int xmax; 
	int ymin; int ymax;
};

// Net
struct s_net { 
	std::string name; 
	int num_cells; int *blocks; 
       	struct s_bb bounding_box; 
};

// Block
struct s_block {
	int name; enum e_block_types type; 
	std::vector<int> nets_in_cell; 
	int x; int y; 
	bool is_movable;  
}; 

